import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SideMenuComponent} from './components/side-menu/side-menu.component';
import {TopNavMenuComponent} from './components/top-nav-menu/top-nav-menu.component';
import {RouterModule, Routes} from '@angular/router';
import {CobaComponent} from './components/contents/coba/coba.component';
import {StatisticsComponent} from './components/contents/statistics/statistics.component';
import {DashboardComponent} from './components/contents/dashboard/dashboard.component';
import {DevicesComponent} from './components/contents/devices/devices.component';
import {ScheduleComponent} from './components/contents/schedule/schedule.component';
import {IftttComponent} from './components/contents/ifttt/ifttt.component';
import {UserManagementComponent} from './components/contents/user-management/user-management.component';
import {ProfileComponent} from './components/contents/profile/profile.component';
import {DetailDashboardComponent} from './components/contents/dashboard/detail-dashboard/detail-dashboard.component';
import {SensorComponent} from './components/contents/devices/sensor/sensor.component';
import {ControlComponent} from './components/contents/devices/control/control.component';
import {CctvComponent} from './components/contents/devices/cctv/cctv.component';
import {AuthenticationService} from './services/authentication.service';
import {HttpModule} from '@angular/http';
import {HttpRequestService} from './services/http-request.service';

const appRoutes: Routes = [
  {
    path: '',
    component: CobaComponent
  },
  {
    path: 'statistics',
    component: StatisticsComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'detail-dashboard',
    component: DetailDashboardComponent
  },
  {
    path: 'devices',
    component: DevicesComponent
  },
  {
    path: 'schedule',
    component: ScheduleComponent
  },
  {
    path: 'ifttt',
    component: IftttComponent
  },
  {
    path: 'user-management',
    component: UserManagementComponent
  }
];


@NgModule({
  declarations: [
    AppComponent,
    SideMenuComponent,
    TopNavMenuComponent,
    CobaComponent,
    StatisticsComponent,
    DashboardComponent,
    DevicesComponent,
    ScheduleComponent,
    IftttComponent,
    UserManagementComponent,
    ProfileComponent,
    DetailDashboardComponent,
    SensorComponent,
    ControlComponent,
    CctvComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthenticationService, HttpRequestService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
