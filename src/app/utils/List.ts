export class List<T> {
  private _items: Array<T>;

  constructor(items: Array<T>) {
    this._items = items;
  }

  size(): number {
    return this._items.length;
  }

  add(value: T): void {
    this._items.push(value);
  }

  get(index: number): T {
    return this._items[index];
  }

  remove(index: number): void {
    this._items.splice(index, 1);
  }

  get itemsArray(): Array<T> {
    return this._items;
  }
}
