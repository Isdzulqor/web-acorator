export class Constanta {
  public static tokenKey = 'tokenAuth';
  public static urlHost = 'http://localhost:3000';
  public static uiUserLogin = Constanta.urlHost + '/api/user/post';
  public static uiDashboard= Constanta.urlHost + '/api/dashboard/all';
}
