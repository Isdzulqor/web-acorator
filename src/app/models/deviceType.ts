export class DeviceType {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private name: string;
  private description: string;


  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }
}
