import {Dashboard} from './dashboard';

export class IfttRule {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _dashboard: Dashboard;
  private _ifttSetting: string;
  private _state: string;

  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get dashboard(): Dashboard {
    return this._dashboard;
  }

  set dashboard(value: Dashboard) {
    this._dashboard = value;
  }

  get ifttSetting(): string {
    return this._ifttSetting;
  }

  set ifttSetting(value: string) {
    this._ifttSetting = value;
  }

  get state(): string {
    return this._state;
  }

  set state(value: string) {
    this._state = value;
  }
}
