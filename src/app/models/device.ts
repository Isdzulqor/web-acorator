import {Dashboard} from './dashboard';
import {DeviceType} from './deviceType';

export class Device {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _code: string;
  private _name: string;
  private _description: string;
  private _state: string;
  private _lastConnection: Date;
  private _dashboard: Dashboard;
  private _user: string;
  private _condition: string;
  private _deviceType: DeviceType;


  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get code(): string {
    return this._code;
  }

  set code(value: string) {
    this._code = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get state(): string {
    return this._state;
  }

  set state(value: string) {
    this._state = value;
  }

  get lastConnection(): Date {
    return this._lastConnection;
  }

  set lastConnection(value: Date) {
    this._lastConnection = value;
  }

  get dashboard(): Dashboard {
    return this._dashboard;
  }

  set dashboard(value: Dashboard) {
    this._dashboard = value;
  }

  get user(): string {
    return this._user;
  }

  set user(value: string) {
    this._user = value;
  }

  get condition(): string {
    return this._condition;
  }

  set condition(value: string) {
    this._condition = value;
  }

  get deviceType(): DeviceType {
    return this._deviceType;
  }

  set deviceType(value: DeviceType) {
    this._deviceType = value;
  }
}
