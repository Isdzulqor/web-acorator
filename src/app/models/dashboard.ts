export class Dashboard {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _name: string;
  private _address: string;
  private _latitude: number;
  private _longitude: number;
  private _ipHost: string;


  constructor(createdBy: string, createdDate: Date, updatedBy: string, updatedDate: Date, id: string, name: string, address: string, latitude: number, longitude: number, ipHost: string) {
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._updatedBy = updatedBy;
    this._updatedDate = updatedDate;
    this._id = id;
    this._name = name;
    this._address = address;
    this._latitude = latitude;
    this._longitude = longitude;
    this._ipHost = ipHost;
  }

  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get address(): string {
    return this._address;
  }

  set address(value: string) {
    this._address = value;
  }

  get latitude(): number {
    return this._latitude;
  }

  set latitude(value: number) {
    this._latitude = value;
  }

  get longitude(): number {
    return this._longitude;
  }

  set longitude(value: number) {
    this._longitude = value;
  }

  get ipHost(): string {
    return this._ipHost;
  }

  set ipHost(value: string) {
    this._ipHost = value;
  }
}
