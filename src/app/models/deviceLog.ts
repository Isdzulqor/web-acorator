import {Device} from './device';

export class DeviceLog {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _value: string;
  private _device: Device;


  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
  }

  get device(): Device {
    return this._device;
  }

  set device(value: Device) {
    this._device = value;
  }
}
