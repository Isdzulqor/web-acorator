import {Schedule} from './schedule';
import {Device} from './device';

export class DetailSchedule {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _state: string;
  private _startDateTime: Date;
  private _endDateTime: Date;
  private _schedule: Schedule;
  private _device: Device;

  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get state(): string {
    return this._state;
  }

  set state(value: string) {
    this._state = value;
  }

  get startDateTime(): Date {
    return this._startDateTime;
  }

  set startDateTime(value: Date) {
    this._startDateTime = value;
  }

  get endDateTime(): Date {
    return this._endDateTime;
  }

  set endDateTime(value: Date) {
    this._endDateTime = value;
  }

  get schedule(): Schedule {
    return this._schedule;
  }

  set schedule(value: Schedule) {
    this._schedule = value;
  }

  get device(): Device {
    return this._device;
  }

  set device(value: Device) {
    this._device = value;
  }
}
