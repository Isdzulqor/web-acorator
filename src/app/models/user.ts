import {List} from '../utils/List';
import {Accessibility} from './accessibility';

export class User {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _email: string;
  private _fullname: string;
  private _phoneNumber: string;
  private _password: string;
  private _tempPassword: string;
  private _manager: string;
  private _role: string;
  private _accessilities: List<Accessibility>;

  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get fullname(): string {
    return this._fullname;
  }

  set fullname(value: string) {
    this._fullname = value;
  }

  get phoneNumber(): string {
    return this._phoneNumber;
  }

  set phoneNumber(value: string) {
    this._phoneNumber = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get tempPassword(): string {
    return this._tempPassword;
  }

  set tempPassword(value: string) {
    this._tempPassword = value;
  }

  get manager(): string {
    return this._manager;
  }

  set manager(value: string) {
    this._manager = value;
  }

  get role(): string {
    return this._role;
  }

  set role(value: string) {
    this._role = value;
  }

  get accessilities(): List<Accessibility> {
    return this._accessilities;
  }

  set accessilities(value: List<Accessibility>) {
    this._accessilities = value;
  }
}
