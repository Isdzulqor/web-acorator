import {Device} from './device';

export class DetailIfttRule {
  private _createdBy: string;
  private _createdDate: Date;
  private _updatedBy: string;
  private _updatedDate: Date;
  private _id: string;
  private _device: Device;
  private _state: string;
  private _deviceCondition: string;
  private _longtime: number;
  private _underValue: number;
  private _overValue: number;
  private _startTime: Date;
  private _endTime: Date;
  private _days: number;
  private _isAnytime: boolean;

  get createdBy(): string {
    return this._createdBy;
  }

  set createdBy(value: string) {
    this._createdBy = value;
  }

  get createdDate(): Date {
    return this._createdDate;
  }

  set createdDate(value: Date) {
    this._createdDate = value;
  }

  get updatedBy(): string {
    return this._updatedBy;
  }

  set updatedBy(value: string) {
    this._updatedBy = value;
  }

  get updatedDate(): Date {
    return this._updatedDate;
  }

  set updatedDate(value: Date) {
    this._updatedDate = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get device(): Device {
    return this._device;
  }

  set device(value: Device) {
    this._device = value;
  }

  get state(): string {
    return this._state;
  }

  set state(value: string) {
    this._state = value;
  }

  get deviceCondition(): string {
    return this._deviceCondition;
  }

  set deviceCondition(value: string) {
    this._deviceCondition = value;
  }

  get longtime(): number {
    return this._longtime;
  }

  set longtime(value: number) {
    this._longtime = value;
  }

  get underValue(): number {
    return this._underValue;
  }

  set underValue(value: number) {
    this._underValue = value;
  }

  get overValue(): number {
    return this._overValue;
  }

  set overValue(value: number) {
    this._overValue = value;
  }

  get startTime(): Date {
    return this._startTime;
  }

  set startTime(value: Date) {
    this._startTime = value;
  }

  get endTime(): Date {
    return this._endTime;
  }

  set endTime(value: Date) {
    this._endTime = value;
  }

  get days(): number {
    return this._days;
  }

  set days(value: number) {
    this._days = value;
  }

  get isAnytime(): boolean {
    return this._isAnytime;
  }

  set isAnytime(value: boolean) {
    this._isAnytime = value;
  }
}
