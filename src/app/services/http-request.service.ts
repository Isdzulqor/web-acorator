import {EventEmitter, Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {User} from '../models/user';
import {Constanta} from '../utils/constanta';
import {Observable} from 'rxjs/Observable';
import {Dashboard} from '../models/dashboard';
import {AuthenticationService} from './authentication.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpRequestService {
  request$: EventEmitter<any>;

  private headers = new Headers({
    'Content-Type': 'application/json'
  });
  // private headers = new Headers({
  //   'Content-Type': 'application/json',
  //   'Authorization': this.authService.getToken().token
  // });

  constructor(private http: Http, private authService: AuthenticationService) {
  }

  login(user: User): Promise<User> {
    return this.http.post(Constanta.uiUserLogin, JSON.stringify(user), {headers: this.headers})
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  logout() {

  }

  uiStatistic(): Observable<Dashboard[]> {
    let dashboardVariable: [Dashboard];

    let promiseDashboard: Promise<Dashboard> = this.http.post(Constanta.uiDashboard, null, {headers: this.headers})
      .toPromise()
      .then(response => {
        console.log("JSON.stringify(response.json()) toPromise : "+JSON.stringify(response.json()));
        //tes
        dashboardVariable = response.json().data;
        return response.json().data as [Dashboard];
      })
      .catch(this.handleError);

    console.log("promiseDashboard : "+ promiseDashboard);
    console.log("JSON.stringfy(promiseDashboard) : "+ JSON.stringify(promiseDashboard));
    console.log("JSON.stringfy(dashboardVariable) : "+ JSON.stringify(dashboardVariable));

    promiseDashboard.then(value => {
      console.log("value : "+value);
      console.log("JSON.stringify(value) : "+JSON.stringify(value));
    });

    this.http.post(Constanta.uiDashboard, null, {headers: this.headers})
      .map(res => res.json())
      .subscribe(res => {
        console.log("res.data : "+res.data);
        console.log("JSON.stringify(res.data[0]) : "+JSON.stringify(res.data[0]));
        console.log("res.data[0].name : "+res.data[0].name);
        console.log("res : "+res);
      });

    // return this.http.post(Constanta.uiDashboard, {headers: this.headers})
    return this.http.post(Constanta.uiDashboard, null,{headers: this.headers})
      .map(res => {
        console.log('res : ' + res);
        console.log('res.json : ' + res.json);

        return res.json().results.map(item => {
          console.log('item :' + item);
          console.log('item.json' + item.json);
        });
      });
  }

  // uiStatistic(): Observable<Dashboard[]> {
  //   return this.http.post(Constanta.uiDashboard, {headers: this.headers})
  //     .map(res => {
  //       return res.json().results.map(item => {
  //         return new Dashboard(
  //           item.data.created_by,
  //           item.data.created_date,
  //           item.data.updated_by,
  //           item.data.updated_date,
  //           item.data._id,
  //           item.data.name,
  //           item.data.address,
  //           item.data.latitude,
  //           item.data.longitude,
  //           item.data.ip_host
  //         );
  //       });
  //     });
  // }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
