import {Injectable} from '@angular/core';
import {Token} from '../models/token';
import {Constanta} from '../utils/constanta';

@Injectable()
export class AuthenticationService {
  public token: Token;

  constructor() {
  }

  storeToken(token: string, userId: string): void {
    const tokenClass: Token = new Token();
    tokenClass.token = token;
    tokenClass.userId = userId;

    localStorage.setItem(Constanta.tokenKey, JSON.stringify(tokenClass));
  }

  getToken(): Token {
    this.token = JSON.parse(localStorage.getItem(Constanta.tokenKey));
    return this.token;
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem(Constanta.tokenKey);
  }
}
