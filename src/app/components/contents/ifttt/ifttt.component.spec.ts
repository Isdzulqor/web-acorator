import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IftttComponent } from './ifttt.component';

describe('IftttComponent', () => {
  let component: IftttComponent;
  let fixture: ComponentFixture<IftttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IftttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IftttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
