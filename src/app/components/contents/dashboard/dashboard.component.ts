import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Dashboard} from '../../../models/dashboard';
import {HttpRequestService} from '../../../services/http-request.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private resultHttp: Observable<Dashboard[]>;

  constructor(private requestService: HttpRequestService) { }
  ngOnInit() {
    this.resultHttp = this.requestService.uiStatistic();


    console.log("resultHttp : "+ JSON.stringify(this.resultHttp));
  }

}
